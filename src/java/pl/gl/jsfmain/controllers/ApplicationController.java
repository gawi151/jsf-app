/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.gl.jsfmain.controllers;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Łukasz
 */
@ManagedBean(name = "app", eager = true)
@ApplicationScoped
public class ApplicationController {

    private String version = "0.0.1";
    private int build = 1;

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the build
     */
    public int getBuild() {
        return build;
    }

    /**
     * @param build the build to set
     */
    public void setBuild(int build) {
        this.build = build;
    }
    
    
    
}
