/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.gl.jsfmain.controllers;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Student
 */
@ManagedBean(name = "auth")
@SessionScoped
public class AuthController implements Serializable{
    
    
    private String username;

    public AuthController() {
        username = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }
    
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "logout";
    }
    
    public String login() {
        return "login";
    }
    
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }
    
}
