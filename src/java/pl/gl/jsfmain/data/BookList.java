/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.gl.jsfmain.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Łukasz
 */
@ManagedBean(name = "book")
@SessionScoped
public class BookList implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final ArrayList<Book> bookList = new ArrayList<Book>(
            Arrays.asList(
            new Book("111111", "Book1",
            new BigDecimal("700.00"), 1),
            new Book("A0002", "Book2",
            new BigDecimal("500.00"), 2),
            new Book("A0003", "Book3",
            new BigDecimal("11600.00"), 8),
            new Book("A0004", "Book4",
            new BigDecimal("5200.00"), 3),
            new Book("A0005", "Book5",
            new BigDecimal("100.00"), 10)));

    public ArrayList<Book> getBookList() {
        return bookList;
    }

    public String saveAction() {
        for (Book book : bookList) {
            book.setEditable(false);
        }
        return null;
    }

    public String editAction(Book book) {
        book.setEditable(true);
        return null;
    }

    public String addAction() {
        bookList.add(new Book("", "", BigDecimal.ZERO, 0));
        return null;
    }
    
    public String deleteAction(Book book) {
        bookList.remove(book);
        return null;
    }

    public static class Book {

        private String bookIsbn;
        private String title;
        private BigDecimal price;
        private int qty;
        private boolean editable;

        public Book(String bookIsbn, String title,
                BigDecimal price, int qty) {

            this.bookIsbn = bookIsbn;
            this.title = title;
            this.price = price;
            this.qty = qty;
        }

        //getter and setter methods
        /**
         * @return the bookIsbn
         */
        public String getBookIsbn() {
            return bookIsbn;
        }

        /**
         * @param bookIsbn the bookIsbn to set
         */
        public void setBookIsbn(String bookIsbn) {
            this.bookIsbn = bookIsbn;
        }

        /**
         * @return the title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title the title to set
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * @return the price
         */
        public BigDecimal getPrice() {
            return price;
        }

        /**
         * @param price the price to set
         */
        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        /**
         * @return the qty
         */
        public int getQty() {
            return qty;
        }

        /**
         * @param qty the qty to set
         */
        public void setQty(int qty) {
            this.qty = qty;
        }

        /**
         * @return the editable
         */
        public boolean isEditable() {
            return editable;
        }

        /**
         * @param editable the editable to set
         */
        public void setEditable(boolean editable) {
            this.editable = editable;
        }
    }
}
